

recette_classique = {
	"pain" : 1/3, // en unités
	"jambon" : 1/6, // en unités
	"fromage" : 1/8, // en unités
	"beurre" : 1/15, // en unités
}


recette = recette_classique



function refresh_quantities() {
	var quantity = parseInt($("#quantity").val(), 10);
	$("#pain").html(Math.ceil(quantity * recette["pain"]));
	$("#jambon").html(Math.ceil(quantity * recette["jambon"]));
	$("#fromage").html(Math.ceil(quantity * recette["fromage"]));
	$("#beurre").html(Math.ceil(quantity * recette["beurre"]));
}




function init() {
	add_plus_moins()
	link_plus_button()
	link_moins_button()
}


function add_plus_moins() {
	// add -/+ buttons to inputs
	$(function() {
	    $("#plusmoins").append('<div class="inc button">+</div><div class="dec button">-</div>');
	});
}


function link_plus_button() {
	$(".button #inc").on("click", function() {
		var $button = $(this);
		var oldValue = $button.parent().find("input").val();
		var newVal = parseFloat(oldValue) + 1;
		$button.parent().find("input").val(newVal);
	});
}


function link_moins_button() {
	$(".button #dec").on("click", function() {
		var $button = $(this);
		var oldValue = $button.parent().find("input").val();

		// Don't allow decrementing below zero
		if (oldValue > 0) {
			var newVal = parseFloat(oldValue) - 1;
		} else {
			newVal = 0;
		}
		$button.parent().find("input").val(newVal);
	});
}


$(document).ready(function() {
	init()
});



