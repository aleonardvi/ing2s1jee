package form;

import database.repositories.User_Repository;

import javax.validation.constraints.NotBlank;
import java.util.Iterator;

public class LoginForm {
    @NotBlank
    private String name;

    @NotBlank
    private String password;

    private User_Repository userRepository;

    // -------------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginForm{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    // -------------------------------------

    public boolean verifForm() {
        boolean res; // Si on a bien toutes les infos justes
        Iterator userTest;

        res = false;
        userTest = userRepository.findAll().iterator();

        while(userTest.hasNext() && !res) {
            res = userTest.equals(this);
            System.out.println(res);
        }

        return res;
    }
}
