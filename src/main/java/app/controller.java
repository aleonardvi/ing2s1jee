package app;

import form.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;

@Controller
public class controller implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/notage").setViewName("notage");
    }

//    @GetMapping("/notage")
//    public String notage() {
//        return "stocks";
//    }
//
//    @GetMapping("/menus")
//    public String menus() {
//        return "menus";
//    }
//
//    @GetMapping("/compta")
//    public String compta() {
//        return "compta";
//    }
//
//    @GetMapping("/historique")
//    public String historique() {
//        return "historique";
//    }

    // -----------------------------

    @GetMapping("/")
    public String showForm(LoginForm logform) {
        return "login";
    }

    @PostMapping("/")
    public String checkLogInfo(@Valid LoginForm logform, BindingResult bindingResult) {

        if (bindingResult.hasErrors() && logform.verifForm()) {
            return "login";
        }

        return "stocks";
    }

    @GetMapping("/error")
    public String error() {
        return "error";
    }

}
