package database;

import javax.persistence.*;

import java.sql.Date;

@Entity
@Table(name = "Daily_Financial_Log")
public class Daily_Financial_Log {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id; // Son id

    @Column()
    private Date date; // la date

    @Column()
    private Float expense; // les dépenses

    @Column()
    private Float income; // les SOUS QU'ON A GAGNES

    // -------------------- WAWOUH --------------------------

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getExpense() {
        return expense;
    }

    public void setExpense(Float expense) {
        this.expense = expense;
    }

    public Float getIncome() {
        return income;
    }

    public void setIncome(Float income) {
        this.income = income;
    }
}