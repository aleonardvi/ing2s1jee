package database;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Sale")
public class Sale {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id; // Son id

    @Column
    private Date date;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
