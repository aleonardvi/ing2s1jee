package database;

import javax.persistence.*;

@Entity
@Table(name = "User")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id; // Son id

    @Column(nullable = false)
    private String name; // Son nom, si joli et musical

    @Column()
    private String password; // Chut, faut pas regarder

    // --------- Les getters et setters générés "aléatoirement" ou je sais pas ---------

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}