package database;

import javax.persistence.*;

@Entity
@Table(name = "Menu")
public class Menu {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id; // Son id

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Float unitaryPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getUnitaryPrice() {
        return unitaryPrice;
    }

    public void setUnitaryPrice(Float unitaryPrice) {
        this.unitaryPrice = unitaryPrice;
    }
}
