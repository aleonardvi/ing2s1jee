package database;

import javax.persistence.*;

@Entity
@Table(name = "Ingredient")
public class Ingredient {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id; // Son id

    @Column(nullable = false)
    private String name;

    @Column
    private Float priceExtimate;

    @Column
    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPriceExtimate() {
        return priceExtimate;
    }

    public void setPriceExtimate(Float priceExtimate) {
        this.priceExtimate = priceExtimate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
