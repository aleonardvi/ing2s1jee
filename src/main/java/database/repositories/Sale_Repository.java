package database.repositories;

import database.Sale;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface Sale_Repository extends CrudRepository<Sale, Integer> {

}