package database.repositories;

import database.Ingredient;
import org.springframework.data.repository.CrudRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface Ingredient_Repository extends CrudRepository<Ingredient, Integer> {

}