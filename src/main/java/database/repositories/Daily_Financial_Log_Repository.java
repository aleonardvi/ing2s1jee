package database.repositories;

import org.springframework.data.repository.CrudRepository;

import database.Daily_Financial_Log;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface Daily_Financial_Log_Repository extends CrudRepository<Daily_Financial_Log, Integer> {

}