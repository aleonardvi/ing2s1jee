package database;

import javax.persistence.*;

@Entity
@Table(name = "IsInto")
public class IsInto {

    @ManyToOne
    @JoinColumn(name = "idMenu") // Le nom que l'on veut de la colonne, ne vous y trompez pas
    private Menu menu; // La table réferencée

    @ManyToOne
    @JoinColumn(name = "idSale")
    private Sale sale;

    @Column(nullable = false)
    private Integer number;

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
