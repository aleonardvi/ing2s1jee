Les pages du projet
===================

Les différentes pages qui composent notre service de "comptabilité de sandwichs".

Page de login
-------------

La page de login permet à l'utilisateur de se logger dans le service. Il a droit de rentrer un login et un mot de passe. Si le login est bon, il est redirigé vers la page de note des commandes de la soirée.

Sur chaque page accessible après le login, l'utilisateur a le possibilité de se délogger et revenir à cette page.

Page de note de la soirée
-------------------------

Ici, l'utilisateur peut noter les sandwichs de la soirée : chaque sandwich correspond à un _menu_, qui a un compteur à côté pour noter le nombre de menus qui sont commandés.

Il peut accéder de cette page :

 * L'inventaire des menus
 * Le récapitulatif monétaire de la soirée
 * L'historique des soirées
 
L'inventaire des _menus_
----------------------

Ici sont regroupés les menus possibles : les différents sandwichs et variantes qui sont proposées. Il sera possible d'en rajouter et d'en supprimer.

Le récapitulatif monétaire de la soirée
---------------------------------------

Comme chaque menu a un coût et une recette, alors on peut en tirer un revenu (ou perte) de la soirée. C'est ce que montre cette page.

L'historique des soirées
------------------------

Cette page regroupe un historique des soirées, livrant un ensemble d'informations comme le revenu global total, sa moyenne, etc. 