Initialiser la BDD pour les Sandwichs
-------------------------------------

Ici se trouve la documentation pour créer la base de donnée et l'utilisateur spring qui va interargir avec.

 * Step 1 : Se logger dans mysql avec un user qui a le droit de créer d'autres utilisateurs :
 ```bash
$ mysql -u votre_puissant_login -p
 ```
 
 * Step 2 : Créer la base de données avec le bon nom :
 ```mysql
CREATE DATABASE db_sandwich;
```

 * Step 3 : Créer l'utilisateur Spring avec ses droits :
```mysql
CREATE USER 'springuser'@'localhost' IDENTIFIED BY 'spring'; -- wow so secure 
GRANT SELECT, INSERT, DELETE, UPDATE ON db_sandwich.* TO 'springuser'@'localhost';
 ```
 
 * Step 4 : Fini !